# About project
Designed and implemented service for online ordering pizza for delivery. Service allows user to select pizza type and size, to order it and to confirm order. The administrator of that service could check and proceed orders, drawing bonuses and prizes between users. 

# What technology I have used?
- Java
- JSF 
- EJB 
- HTML, CSS, Bootstrap