package com.tdea.mkr.model.validation;

import com.tdea.mkr.dao.UserDao;
import com.tdea.mkr.entity.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import java.util.List;

/**
 * Created by olexiy on 11/29/16.
 */
@ManagedBean(name = "existUserValidator")
@RequestScoped
public class UserExistValidator implements Validator{

    private final static Logger logger = LogManager.getLogger();

    @EJB
    private UserDao<User> userDao;

    @Override
    public void validate(FacesContext facesContext, UIComponent uiComponent, Object o) throws ValidatorException {
        String userName = (String) o;
        logger.info("Validate for existing user, check username={" + userName + "}");
        List<User> userFromDB = userDao.find(userName);
        if(!userFromDB.isEmpty()) {
            FacesMessage message = new FacesMessage("User already exist", "User already exist, choose another name");
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            throw new ValidatorException(message);
        }
    }
}
