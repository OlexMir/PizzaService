package com.tdea.mkr.model.validation;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

/**
 * Created by olexiy on 11/28/16.
 */
@FacesConverter(value = "priceConverter", forClass = String.class)
public class PriceConverter implements Converter{

    private static Logger logger = LogManager.getLogger(PriceConverter.class);

    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String s) {
        if(s.isEmpty()) {
            FacesMessage msg = new FacesMessage(
                    "Price Conversion error", "Invalid Price format");
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            throw new ConverterException(msg);
        }
        String result = s.replace(".", "");
        Long value = Long.valueOf(result);
        return value;
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object o) {
        return o.toString();
    }
}
