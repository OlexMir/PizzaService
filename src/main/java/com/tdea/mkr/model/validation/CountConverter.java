package com.tdea.mkr.model.validation;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 * Created by olexiy on 11/21/16.
 */
@FacesConverter(value = "countConverter", forClass = String.class)
public class CountConverter implements Converter {

    private static Logger logger = LogManager.getLogger(CountConverter.class);

    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String s) {
        if(s.isEmpty())
            return 1;
        return Integer.valueOf(s);
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object o) {
        return o.toString();
    }
}
