package com.tdea.mkr.dao;

import com.tdea.mkr.entity.Pizza;

import java.util.List;

/**
 * Created by olexiy on 11/20/16.
 */
public interface PizzaDao<T> extends Dao<T> {
    List<Pizza> getAllPizzas();
}
