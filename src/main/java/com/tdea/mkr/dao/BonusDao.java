package com.tdea.mkr.dao;

import java.util.List;

/**
 * Created by olexiy on 11/28/16.
 */
public interface BonusDao<T> extends Dao<T> {
    List<T> findBonusesByUserId(Long userId);

    List<T> findActiveBonusesByUserId(Long id);
}
