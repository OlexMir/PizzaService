package com.tdea.mkr.dao;

import com.tdea.mkr.entity.UserOrder;

import java.util.List;

/**
 * Created by olexiy on 11/20/16.
 */
public interface OrderDao<T> extends Dao<T> {

    List<T> findOrdersByUserIdAndStatus(Long userId, String status);

    List<T> findAllOrdersByUserId(Long id);

    List<T> findOrdersByStatus(String status);
}
