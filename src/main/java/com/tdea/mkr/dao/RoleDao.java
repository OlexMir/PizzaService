package com.tdea.mkr.dao;

/**
 * Created by olexiy on 11/25/16.
 */
public interface RoleDao<T> extends Dao<T> {
    T find(String roleName);
}
