package com.tdea.mkr.dao.impl;

import com.tdea.mkr.entity.User;
import com.tdea.mkr.dao.UserDao;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * Created by olexiy on 11/25/16.
 */
@Stateless
@Local(UserDao.class)
public class BaseUserDao extends AbstractDao<User> implements UserDao<User> {

    private static Logger logger = LogManager.getLogger(BaseUserDao.class);

    @Override
    public Class<User> getEntityClass() {
        return User.class;
    }

    @Override
    public List<User> find(String userName) {
        logger.info("Find User by login=" + userName);
        String query = "select u from User u where u.login = ?1";
        TypedQuery<User> typedQuery = em.createQuery(query, getEntityClass())
                .setParameter(1, userName);
        return typedQuery.getResultList();
    }

    @Override
    public List<User> getAll() {
        logger.info("Get all users from DB");
        String query = "select u from User u";
        TypedQuery<User> typedQuery = em.createQuery(query, getEntityClass());
        return typedQuery.getResultList();
    }
}
