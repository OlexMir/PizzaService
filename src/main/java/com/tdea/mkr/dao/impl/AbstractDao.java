package com.tdea.mkr.dao.impl;

import com.tdea.mkr.dao.Dao;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Created by olexiy on 11/20/16.
 */
public abstract class AbstractDao<T> implements Dao<T> {

    private static final Logger loggger = LogManager.getLogger(AbstractDao.class);

    @PersistenceContext(unitName = "PizzaEntityManager")
    protected EntityManager em;

    @Override
    public void add(T obj) {
        loggger.info("Persisting object " + obj.toString());
        em.persist(obj);
    }

    @Override
    public void delete(Long id) {
        loggger.info("Removing object by id=" + id);
        em.remove(find(id));
    }

    @Override
    public void delete(T obj) {
        loggger.info("Removing object by reference " + obj.toString());
        em.remove(em.contains(obj) ? obj : em.merge(obj));
    }

    @Override
    public void update(T obj) {
        loggger.info("Updating object " + obj.toString());
        em.merge(obj);
    }

    @Override
    public T find(Long id) {
        loggger.info("Find object " + getEntityClass() + " by id=" + id);
        return em.find(getEntityClass(), id);
    }

    @Override
    public abstract Class<T> getEntityClass();
}
