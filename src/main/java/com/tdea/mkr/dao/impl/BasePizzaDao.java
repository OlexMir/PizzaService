package com.tdea.mkr.dao.impl;

import com.tdea.mkr.dao.PizzaDao;
import com.tdea.mkr.entity.Pizza;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * Created by olexiy on 11/20/16.
 */
@Stateless
@Local(PizzaDao.class)
public class BasePizzaDao extends AbstractDao<Pizza> implements PizzaDao<Pizza> {

    private static Logger logger = LogManager.getLogger(BasePizzaDao.class);

    @Override
    public Class<Pizza> getEntityClass() {
        return Pizza.class;
    }

    @Override
    public List<Pizza> getAllPizzas() {
        String query = "select p from Pizza p";
        TypedQuery<Pizza> typedQuery = em.createQuery(query, getEntityClass());
        return typedQuery.getResultList();
    }
}
