package com.tdea.mkr.dao.impl;

import com.tdea.mkr.dao.BonusDao;
import com.tdea.mkr.entity.Bonus;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * Created by olexiy on 11/28/16.
 */
@Stateless
@Local(BonusDao.class)
public class BaseBonusDao extends AbstractDao<Bonus> implements BonusDao<Bonus> {

    private static final Logger loggger = LogManager.getLogger(BaseBonusDao.class);

    @Override
    public Class<Bonus> getEntityClass() {
        return Bonus.class;
    }

    @Override
    public List<Bonus> findBonusesByUserId(Long userId) {
        loggger.info("Find all bonuses by user id=" + userId);
        String query = "select b from Bonus b where b.user.id = ?1 order by b.active desc";
        TypedQuery<Bonus> typedQuery = em.createQuery(query, getEntityClass())
                .setParameter(1, userId);
        return typedQuery.getResultList();
    }

    @Override
    public List<Bonus> findActiveBonusesByUserId(Long userId) {
        loggger.info("Find all active bonuses by user id=" + userId);
        String query = "select b from Bonus b where b.user.id = ?1 and b.active = true";
        TypedQuery<Bonus> typedQuery = em.createQuery(query, getEntityClass())
                .setParameter(1, userId);
        return typedQuery.getResultList();
    }
}
