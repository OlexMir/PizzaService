package com.tdea.mkr.dao.impl;

import com.tdea.mkr.dao.OrderRowDao;
import com.tdea.mkr.entity.OrderRow;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * Created by olexiy on 11/21/16.
 */
@Stateless
@Local(OrderRowDao.class)
public class BaseOrderRowDao extends AbstractDao<OrderRow> implements OrderRowDao<OrderRow> {

    private static Logger logger = LogManager.getLogger(BaseOrderRowDao.class);

    @Override
    public Class<OrderRow> getEntityClass() {
        return OrderRow.class;
    }

    @Override
    public List<OrderRow> findOrderRowsByOrderId(Long orderId) {
        logger.info("Find all order rows by order id=" + orderId);
        String query = "select row from OrderRow row where row.userOrder.id = ?1";
        TypedQuery<OrderRow> typedQuery = em.createQuery(query, getEntityClass())
                .setParameter(1, orderId);
        return typedQuery.getResultList();
    }

    @Override
    public Long getTotalByOrder(Long orderId) {
        logger.info("Calculate total by order id=" + orderId);
        String query = "select sum (row.total) from OrderRow row where row.userOrder.id = ?1";
        Query sumQuery = em.createQuery(query).setParameter(1, orderId);
        return (Long) sumQuery.getSingleResult();
    }
}
