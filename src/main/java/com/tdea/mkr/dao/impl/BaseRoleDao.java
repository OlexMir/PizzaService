package com.tdea.mkr.dao.impl;

import com.tdea.mkr.entity.Role;
import com.tdea.mkr.dao.RoleDao;
import com.tdea.mkr.entity.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.TypedQuery;

/**
 * Created by olexiy on 11/25/16.
 */
@Stateless
@Local(RoleDao.class)
public class BaseRoleDao extends AbstractDao<Role> implements RoleDao<Role> {

    private static Logger logger = LogManager.getLogger(BaseRoleDao.class);

    @Override
    public Class<Role> getEntityClass() {
        return Role.class;
    }

    @Override
    public Role find(String roleName) {
        logger.info("Find role by name=" + roleName);
        String query = "select r from Role r where r.roleName = ?1";
        TypedQuery<Role> typedQuery = em.createQuery(query, getEntityClass())
                .setParameter(1, roleName);
        return typedQuery.getSingleResult();
    }
}
