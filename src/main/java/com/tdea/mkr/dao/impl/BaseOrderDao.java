package com.tdea.mkr.dao.impl;

import com.tdea.mkr.dao.OrderDao;
import com.tdea.mkr.entity.User;
import com.tdea.mkr.entity.UserOrder;
import com.tdea.mkr.entity.util.OrderStatus;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * Created by olexiy on 11/20/16.
 */
@Stateless
@Local(OrderDao.class)
public class BaseOrderDao extends AbstractDao<UserOrder> implements OrderDao<UserOrder> {

    private static Logger logger = LogManager.getLogger(BaseOrderDao.class);

    @Override
    public Class<UserOrder> getEntityClass() {
        return UserOrder.class;
    }

    @Override
    public List<UserOrder> findOrdersByUserIdAndStatus(Long userId, String status) {
        logger.info("Find orders by user id=" + userId + " and status " + status);
        String query = "select o from UserOrder o where o.userId = ?1 and o.status = ?2 order by o.orderDate desc";
        TypedQuery<UserOrder> typedQuery = em.createQuery(query, getEntityClass())
                .setParameter(1, userId)
                .setParameter(2, status);
        return typedQuery.getResultList();
    }

    @Override
    public List<UserOrder> findAllOrdersByUserId(Long id) {
        logger.info("Find orders by user id=" + id);
        String query = "select o from UserOrder o where o.userId = ?1 order by o.orderDate desc";
        TypedQuery<UserOrder> typedQuery = em.createQuery(query, getEntityClass())
                .setParameter(1, id);
        return typedQuery.getResultList();
    }

    @Override
    public List<UserOrder> findOrdersByStatus(String status) {
        logger.info("Find orders by status " + status);
        String query = "select o from UserOrder o where o.status = ?1 order by o.orderDate desc";
        TypedQuery<UserOrder> typedQuery = em.createQuery(query, getEntityClass())
                .setParameter(1, status);
        return typedQuery.getResultList();
    }
}
