package com.tdea.mkr.dao.impl;

import com.tdea.mkr.dao.SizeDao;
import com.tdea.mkr.entity.Size;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.TypedQuery;

/**
 * Created by olexiy on 11/20/16.
 */
@Stateless
@Local(SizeDao.class)
public class BaseSizeDao extends AbstractDao<Size> implements SizeDao<Size> {

    private static Logger logger = LogManager.getLogger(BaseSizeDao.class);

    @Override
    public Class<Size> getEntityClass() {
        return Size.class;
    }

    @Override
    public Size findByName(String name) {
        logger.info("Find size by name=" + name);
        String query = "select s from Size s where s.value = ?1";
        TypedQuery<Size> typedQuery = em.createQuery(query, getEntityClass())
                .setParameter(1, name);
        return typedQuery.getSingleResult();
    }
}
