package com.tdea.mkr.dao;

import com.tdea.mkr.dao.Dao;
import com.tdea.mkr.entity.Size;

/**
 * Created by olexiy on 11/20/16.
 */
public interface SizeDao<T> extends Dao<T> {
    Size findByName(String name);
}
