package com.tdea.mkr.dao;

import com.tdea.mkr.entity.User;

import java.util.List;

/**
 * Created by olexiy on 11/25/16.
 */
public interface UserDao<T> extends Dao<T> {
    List<T> find(String userName);

    List<T> getAll();
}
