package com.tdea.mkr.dao;

import com.tdea.mkr.entity.OrderRow;

import java.util.List;

/**
 * Created by olexiy on 11/21/16.
 */
public interface OrderRowDao<T> extends Dao<T> {
    List<T> findOrderRowsByOrderId(Long orderId);

    Long getTotalByOrder(Long id);
}
