package com.tdea.mkr.dao;

/**
 * Created by olexiy on 11/20/16.
 */
public interface Dao<T> {
    void add(T obj);

    void delete(Long id);

    void delete(T obj);

    void update(T obj);

    T find(Long id);

    Class<T> getEntityClass();
}
