package com.tdea.mkr.controller;

import com.tdea.mkr.entity.Pizza;
import com.tdea.mkr.entity.Size;
import com.tdea.mkr.model.Item;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.event.AjaxBehaviorEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by olexiy on 11/20/16.
 */
@ManagedBean
@SessionScoped
public class ShoppingCart {

    private static final Logger logger = LogManager.getLogger(ShoppingCart.class);

    private HashMap<Item, Integer> pizzasInCart;

    public ShoppingCart() {
        pizzasInCart = new LinkedHashMap<>();
    }

    public Item createShopCartItem(Pizza pizza, Size size) {
        logger.info("Create new item in cart");
        Item item = new Item(
                pizza.getId(),
                pizza.getName(),
                size.getValue(),
                pizza.getPrices().get(size)
        );
        return item;
    }

    public int getShoppingCartSize() {
        int result = 0;
        for(Item item : pizzasInCart.keySet()) {
            result += pizzasInCart.get(item);
        }
        return result;
    }

    public void add(Item item) {
        logger.info("Add new item to cart" + item.toString());
        if(pizzasInCart.get(item) != null) {
            int value = pizzasInCart.get(item);
            value++;
            pizzasInCart.put(item, value);
        } else {
            pizzasInCart.put(item, 1);
        }
    }

    public void remove(Item item) {
        logger.info("Remove item from cart " + item.toString());
        pizzasInCart.remove(item);
    }

    public Long calculateTotalItemPrice(Item item) {
        long count = pizzasInCart.get(item);
        long price = item.getPrice();
        return price * count;
    }

    public void incrementItemCount(Item item) {
        int value = pizzasInCart.get(item);
        if(value == 9999)
            return;
        value++;
        pizzasInCart.put(item, value);
    }

    public void decrementItemCount(Item item) {
        int value = pizzasInCart.get(item);
        if(value == 1)
            return;
        value--;
        pizzasInCart.put(item, value);
    }

    public Integer getValue(Item item) {
        return pizzasInCart.get(item);
    }

    public List<Item> getItemKeyList() {
        return new ArrayList<>(pizzasInCart.keySet());
    }

    public HashMap<Item, Integer> getPizzasInCart() {
        return pizzasInCart;
    }

    public void setPizzasInCart(HashMap<Item, Integer> pizzasInCart) {
        this.pizzasInCart = pizzasInCart;
    }


}
