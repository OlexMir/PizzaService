package com.tdea.mkr.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

/**
 * Created by olexiy on 11/20/16.
 */
@ManagedBean(name = "navigationController", eager = true)
@RequestScoped
public class NavigationController {

    private static Logger logger = LogManager.getLogger(NavigationController.class);

    public String navigateToShoppingCartPage() {
        return "/main/shopping-cart-page?faces-redirect=true";
    }

    public String navigateToIndex() {
        return "/index?faces-redirect=true";
    }

    public String navigateToOrderForm() {
        return "/main/order-form-page?faces-redirect=true";
    }

    public String navigateToLoginPage() {
        return "login?faces-redirect=true";
    }

    public String navigateToErrorLoginPage() {
        return "error-login?faces-redirect=true";
    }

    public String navigateToRegistrationPage() {
        return "/main/registration-page?faces-redirect=true";
    }

    public String navigateToPizzaAddPage() {
        return "/admin/pizza-add-page?faces-redirect=true";
    }

    public String navigateToPizzaEditPage(Integer id) {
        return "/admin/pizza-edit-page?id=" + id + "&faces-redirect=true";
    }

    public String navigateToUserCabinet() {
        return "/user/user-cabinet.xhtml?faces-redirect=true";
    }

    public String navigateToAdminCabinet() {
        return "/admin/admin-cabinet.xhtml?faces-redirect=true";
    }

    public String navigateToBonusPage() {
        return "/jms/bonus-page.xhtml?faces-redirect=true";
    }

    public String navigateToEditBonusPage() {
        return "/jms/bonus-page-edit.xhtml?faces-redirect=true";
    }
}
