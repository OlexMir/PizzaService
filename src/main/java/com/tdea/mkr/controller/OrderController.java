package com.tdea.mkr.controller;

import com.tdea.mkr.dao.BonusDao;
import com.tdea.mkr.dao.OrderRowDao;
import com.tdea.mkr.dao.UserDao;
import com.tdea.mkr.entity.OrderRow;
import com.tdea.mkr.entity.UserOrder;
import com.tdea.mkr.entity.User;
import com.tdea.mkr.entity.util.OrderStatus;
import com.tdea.mkr.entity.Bonus;
import com.tdea.mkr.model.Item;
import com.tdea.mkr.service.OrderService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import java.util.List;
import java.util.Map;

/**
 * Created by olexiy on 11/21/16.
 */
@ManagedBean
@RequestScoped
public class OrderController {

    private static final Logger logger = LogManager.getLogger(OrderController.class);

    @EJB
    private OrderService orderService;

    @EJB
    private OrderRowDao<OrderRow> orderRowDao;

    @EJB
    private UserDao<User> userDao;

    @EJB
    private BonusDao<Bonus> bonusDao;

    private UserOrder userOrder;

    private User user;

    private Long bonusId;

    @PostConstruct
    public void init() {
        String userName = FacesContext.getCurrentInstance().getExternalContext().getRemoteUser();
        if(userName != null) {
            user = userDao.find(userName).get(0);
        }
        userOrder = new UserOrder();
    }

    public void createOrder(Map<Item, Integer> pizzasInCart) {
        userOrder.setStatus(OrderStatus.PROCESSING.toString());
        userOrder.setUserId(user == null ? null : user.getId());
        if(bonusId != null) {
             userOrder.setBonus(bonusDao.find(bonusId));
        } else {
            userOrder.setBonus(null);
        }
        orderService.createOrder(userOrder, pizzasInCart);
    }

    public List<UserOrder> getOrders() {
        logger.info("Get all orders for user id=" + user.getId());
        return orderService.findAllOrders(user.getId());
    }

    public List<UserOrder> getProcessingOrders() {
        return orderService.findAllProcessingOrders();
    }

    public List<UserOrder> getConfirmedOrders() {
        return orderService.findAllConfirmedOrders();
    }

    public List<OrderRow> getOrderRowForOrder(Long userOrderId) {
        return orderRowDao.findOrderRowsByOrderId(userOrderId);
    }

    public void confirmOrder(Long userOrderId) {
        logger.info("Try to confirm order id=" + userOrderId);
        orderService.confirmOrder(userOrderId);
    }

    public void cancelOrder(Long userOrderId) {
        logger.info("Try to cancel order id=" + userOrderId);
        orderService.cancelOrder(userOrderId);
    }

    public List<Bonus> getActiveBonusesByUser() {
        return bonusDao.findActiveBonusesByUserId(user.getId());
    }

    public OrderService getOrderService() {
        return orderService;
    }

    public void setOrderService(OrderService orderService) {
        this.orderService = orderService;
    }

    public UserOrder getUserOrder() {
        return userOrder;
    }

    public void setUserOrder(UserOrder userOrder) {
        this.userOrder = userOrder;
    }

    public UserDao<User> getUserDao() {
        return userDao;
    }

    public void setUserDao(UserDao<User> userDao) {
        this.userDao = userDao;
    }

    public OrderRowDao<OrderRow> getOrderRowDao() {
        return orderRowDao;
    }

    public void setOrderRowDao(OrderRowDao<OrderRow> orderRowDao) {
        this.orderRowDao = orderRowDao;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public BonusDao<Bonus> getBonusDao() {
        return bonusDao;
    }

    public void setBonusDao(BonusDao<Bonus> bonusDao) {
        this.bonusDao = bonusDao;
    }

    public Long getBonusId() {
        return bonusId;
    }

    public void setBonusId(Long bonusId) {
        this.bonusId = bonusId;
    }
}
