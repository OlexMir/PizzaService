package com.tdea.mkr.controller;

import com.tdea.mkr.dao.RoleDao;
import com.tdea.mkr.dao.UserDao;
import com.tdea.mkr.entity.Role;
import com.tdea.mkr.entity.User;
import com.tdea.mkr.entity.util.RoleName;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jboss.security.Base64Encoder;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

/**
 * Created by olexiy on 11/27/16.
 */
@ManagedBean
@RequestScoped
public class RegistrationController {

    private static Logger logger = LogManager.getLogger();

    @EJB
    private UserDao<User> userDao;

    @EJB
    private RoleDao<Role> roleDao;

    @ManagedProperty(value = "#{authController}")
    private AuthController authController;

    private String userName;

    private String password;

    public void register() {
        logger.info("Validator check for existing user");

        logger.info("Create new user");
        User user = new User();
        user.setLogin(userName);
        user.setPassword(encodePassword(password));
        user.setRole(roleDao.find(RoleName.ROLE_USER.toString()));
        userDao.add(user);

        logger.info("Do login for new user");
        authController.setUserName(userName);
        authController.setPassword(password);
        authController.doLogin();
    }

    private String encodePassword(String password) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            digest.update(password.getBytes());
            return Base64Encoder.encode(digest.digest());
        } catch (NoSuchAlgorithmException e) {
            logger.error("No such encryption algorithm was found", e);
        } catch (IOException e) {
            logger.error("Cant encrypt password", e);
        }
        return password;
    }

    public UserDao<User> getUserDao() {
        return userDao;
    }

    public void setUserDao(UserDao<User> userDao) {
        this.userDao = userDao;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public RoleDao<Role> getRoleDao() {
        return roleDao;
    }

    public void setRoleDao(RoleDao<Role> roleDao) {
        this.roleDao = roleDao;
    }

    public AuthController getAuthController() {
        return authController;
    }

    public void setAuthController(AuthController authController) {
        this.authController = authController;
    }
}
