package com.tdea.mkr.controller;

import com.tdea.mkr.dao.BonusDao;
import com.tdea.mkr.dao.OrderDao;
import com.tdea.mkr.dao.OrderRowDao;
import com.tdea.mkr.dao.UserDao;
import com.tdea.mkr.entity.UserOrder;
import com.tdea.mkr.entity.OrderRow;
import com.tdea.mkr.entity.User;
import com.tdea.mkr.entity.Bonus;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import java.util.List;

/**
 * Created by olexiy on 11/27/16.
 */
@ManagedBean
@RequestScoped
public class UserCabinetController {

    private static Logger logger = LogManager.getLogger(UserCabinetController.class);

    private String currentView = "bonuses";

    @ManagedProperty(value = "#{orderController}")
    private OrderController orderController;

    @EJB
    private UserDao<User> userDao;

    @EJB
    private OrderDao<UserOrder> orderDao;

    @EJB
    private OrderRowDao<OrderRow> orderRowDao;

    @EJB
    private BonusDao<Bonus> bonusDao;

    private User user;

    @PostConstruct
    public void init() {
        user = getUser();
        logger.info(user);
    }

    public User getUser() {
        return userDao.find(getUserName()).get(0);
    }

    public String getUserName() {
        logger.info(FacesContext.getCurrentInstance().getExternalContext().getRemoteUser());
        return FacesContext.getCurrentInstance().getExternalContext().getRemoteUser();
    }

    public List<Bonus> getBonuses() {
        List<Bonus> list = bonusDao.findBonusesByUserId(user.getId());
        for (Bonus b : list) {
            logger.info(b.toString());
        }
        return list;
    }

    public String getCurrentView() {
        return currentView;
    }

    public void setCurrentView(String currentView) {
        this.currentView = currentView;
    }

    public UserDao<User> getUserDao() {
        return userDao;
    }

    public void setUserDao(UserDao<User> userDao) {
        this.userDao = userDao;
    }

    public OrderDao<UserOrder> getOrderDao() {
        return orderDao;
    }

    public void setOrderDao(OrderDao<UserOrder> orderDao) {
        this.orderDao = orderDao;
    }

    public OrderRowDao<OrderRow> getOrderRowDao() {
        return orderRowDao;
    }

    public void setOrderRowDao(OrderRowDao<OrderRow> orderRowDao) {
        this.orderRowDao = orderRowDao;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public OrderController getOrderController() {
        return orderController;
    }

    public void setOrderController(OrderController orderController) {
        this.orderController = orderController;
    }
}
