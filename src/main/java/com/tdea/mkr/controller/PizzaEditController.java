package com.tdea.mkr.controller;

import com.tdea.mkr.dao.PizzaDao;
import com.tdea.mkr.dao.SizeDao;
import com.tdea.mkr.entity.Pizza;
import com.tdea.mkr.entity.Size;
import com.tdea.mkr.entity.util.SizeValue;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.faces.view.facelets.FaceletException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by olexiy on 11/27/16.
 */
@ManagedBean
@RequestScoped
public class PizzaEditController {

    private static Logger logger = LogManager.getLogger(PizzaEditController.class);

    @EJB
    private PizzaDao<Pizza> pizzaDao;

    @EJB
    private SizeDao<Size> sizeDao;

    private Pizza pizza;

    private Long priceFor30;

    private Long priceFor40;

    private Size size30;

    private Size size40;

    @PostConstruct
    public void init() {
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        long pizzaId = Integer.parseInt(params.get("id"));

        pizza = pizzaDao.find(pizzaId);
        if(pizza == null) {
            logger.error("Cant find pizza with this url path");
            throw new FaceletException("No pizza with such id");
        } else {
            size30 = sizeDao.findByName(SizeValue.DIAMETER_30.toString());
            size40 = sizeDao.findByName(SizeValue.DIAMETER_40.toString());

            priceFor30 = pizza.getPrices().get(size30);
            priceFor40 = pizza.getPrices().get(size40);
        }
    }

    public void editPizza() {
        logger.info("Try to edit pizza with id=" + pizza.getId());

        Map<Size, Long> prices = new HashMap<>();

        prices.put(size30, priceFor30);
        prices.put(size40, priceFor40);
        pizza.setPrices(prices);

        pizzaDao.update(pizza);

        logger.info("Pizza edited " + pizza.toString());
    }

    public PizzaDao<Pizza> getPizzaDao() {
        return pizzaDao;
    }

    public void setPizzaDao(PizzaDao<Pizza> pizzaDao) {
        this.pizzaDao = pizzaDao;
    }

    public SizeDao<Size> getSizeDao() {
        return sizeDao;
    }

    public void setSizeDao(SizeDao<Size> sizeDao) {
        this.sizeDao = sizeDao;
    }

    public Pizza getPizza() {
        return pizza;
    }

    public void setPizza(Pizza pizza) {
        this.pizza = pizza;
    }

    public Long getPriceFor30() {
        return priceFor30;
    }

    public void setPriceFor30(Long priceFor30) {
        this.priceFor30 = priceFor30;
    }

    public Long getPriceFor40() {
        return priceFor40;
    }

    public void setPriceFor40(Long priceFor40) {
        this.priceFor40 = priceFor40;
    }
}
