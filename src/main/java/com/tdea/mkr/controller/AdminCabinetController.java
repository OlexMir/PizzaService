package com.tdea.mkr.controller;

import com.tdea.mkr.dao.OrderDao;
import com.tdea.mkr.dao.OrderRowDao;
import com.tdea.mkr.dao.UserDao;
import com.tdea.mkr.entity.Pizza;
import com.tdea.mkr.entity.UserOrder;
import com.tdea.mkr.entity.OrderRow;
import com.tdea.mkr.entity.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jboss.security.Base64Encoder;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

/**
 * Created by olexiy on 11/27/16.
 */
@ManagedBean
@RequestScoped
public class AdminCabinetController {

    private static Logger logger = LogManager.getLogger(AdminCabinetController.class);

    private String currentView = "processingOrders";

    @ManagedProperty(value = "#{orderController}")
    private OrderController orderController;

    @EJB
    private UserDao<User> userDao;

    public List<User> getAllUser() {
        return userDao.getAll();
    }

    public UserDao<User> getUserDao() {
        return userDao;
    }

    public void setUserDao(UserDao<User> userDao) {
        this.userDao = userDao;
    }

    public String getCurrentView() {
        return currentView;
    }

    public void setCurrentView(String currentView) {
        this.currentView = currentView;
    }

    public OrderController getOrderController() {
        return orderController;
    }

    public void setOrderController(OrderController orderController) {
        this.orderController = orderController;
    }
}
