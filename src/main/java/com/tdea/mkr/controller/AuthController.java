package com.tdea.mkr.controller;

import com.tdea.mkr.dao.UserDao;
import com.tdea.mkr.entity.User;
import com.tdea.mkr.entity.util.RoleName;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

/**
 * Created by olexiy on 11/25/16.
 */
@ManagedBean(eager = true)
@RequestScoped
public class AuthController {

    private static Logger logger = LogManager.getLogger(AuthController.class);

    @EJB
    private UserDao<User> userDao;

    @ManagedProperty(value = "#{navigationController}")
    private NavigationController navigationController;

    private String userName;

    private String password;

    public String doLogin() {
        HttpServletRequest request = getRequest();
        try {
            request.login(userName, password);
        } catch (ServletException e) {
            logger.error("Login failed", e);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Login failed"));
            return navigationController.navigateToErrorLoginPage();
        }
        return navigationController.navigateToIndex();
    }

    public String doLogout() {
        getRequest().getSession().invalidate();
        return navigationController.navigateToIndex();
    }

    public String toLogin() {
        return navigationController.navigateToLoginPage();
    }

    public boolean isLoggedIn() {
        return FacesContext.getCurrentInstance().getExternalContext().getRemoteUser() != null;
    }

    public boolean isAdmin() {
        return getRequest().isUserInRole(RoleName.ROLE_ADMIN.toString());
    }

    public boolean isUser() {
        return getRequest().isUserInRole(RoleName.ROLE_USER.toString());
    }

    private HttpServletRequest getRequest() {
        return (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserDao<User> getUserDao() {
        return userDao;
    }

    public void setUserDao(UserDao<User> userDao) {
        this.userDao = userDao;
    }

    public NavigationController getNavigationController() {
        return navigationController;
    }

    public void setNavigationController(NavigationController navigationController) {
        this.navigationController = navigationController;
    }
}
