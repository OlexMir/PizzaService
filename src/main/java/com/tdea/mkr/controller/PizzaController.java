package com.tdea.mkr.controller;

import com.tdea.mkr.dao.PizzaDao;
import com.tdea.mkr.dao.SizeDao;
import com.tdea.mkr.entity.Pizza;
import com.tdea.mkr.entity.Size;
import com.tdea.mkr.entity.util.SizeValue;
import com.tdea.mkr.model.Item;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by olexiy on 11/20/16.
 */
@ManagedBean
@RequestScoped
public class PizzaController {

    private static Logger logger = LogManager.getLogger(PizzaController.class);

    @EJB
    private PizzaDao<Pizza> pizzaDao;

    @EJB
    private SizeDao<Size> sizeDao;

    private Pizza pizza;
    private Long priceFor30;
    private Long priceFor40;

    @PostConstruct
    private void init() {
        pizza = new Pizza();
    }

    public List<Pizza> getAllPizzas() {
        return pizzaDao.getAllPizzas();
    }

    public List<Size> getSizeKeyList(Pizza pizza) {
        return new ArrayList(pizza.getPrices().keySet());
    }

    public void savePizza() {
        logger.info("Try to save new pizza to DB");

        Map<Size, Long> prices = new HashMap<>();

        Size size30 = sizeDao.findByName(SizeValue.DIAMETER_30.toString());
        Size size40 = sizeDao.findByName(SizeValue.DIAMETER_40.toString());

        prices.put(size30, priceFor30);
        prices.put(size40, priceFor40);
        pizza.setPrices(prices);

        pizzaDao.add(pizza);

        logger.info("New pizza saved " + pizza.toString());
    }

    public void removePizza(Long pizzaId) {
        pizzaDao.delete(pizzaId);
    }

    public PizzaDao<Pizza> getPizzaDao() {
        return pizzaDao;
    }

    public void setPizzaDao(PizzaDao<Pizza> pizzaDao) {
        this.pizzaDao = pizzaDao;
    }

    public Pizza getPizza() {
        return pizza;
    }

    public void setPizza(Pizza pizza) {
        this.pizza = pizza;
    }

    public Long getPriceFor30() {
        return priceFor30;
    }

    public void setPriceFor30(Long priceFor30) {
        this.priceFor30 = priceFor30;
    }

    public Long getPriceFor40() {
        return priceFor40;
    }

    public void setPriceFor40(Long priceFor40) {
        this.priceFor40 = priceFor40;
    }

    public SizeDao<Size> getSizeDao() {
        return sizeDao;
    }

    public void setSizeDao(SizeDao<Size> sizeDao) {
        this.sizeDao = sizeDao;
    }
}
