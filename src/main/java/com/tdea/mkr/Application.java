package com.tdea.mkr;

import com.tdea.mkr.dao.*;
import com.tdea.mkr.entity.Role;
import com.tdea.mkr.entity.User;
import com.tdea.mkr.entity.Pizza;
import com.tdea.mkr.entity.Size;
import com.tdea.mkr.entity.util.RoleName;
import com.tdea.mkr.entity.util.SizeValue;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jboss.security.Base64Encoder;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Created by olexiy on 11/20/16.
 */
@ManagedBean(eager = true)
@ApplicationScoped
public class Application {

    public static final Logger loggger = LogManager.getLogger(Application.class);

    @EJB
    private SizeDao<Size> sizeDao;

    @EJB
    private PizzaDao<Pizza> pizzaDao;

    @EJB
    private UserDao<User> userDao;

    @EJB
    private RoleDao<Role> roleDao;

    @PostConstruct
    public void init() {
        Logger logger = LogManager.getLogger();
        logger.info("Hello, world");

//        Size size1 = new Size(SizeValue.DIAMETER_30.toString());
//        Size size2 = new Size(SizeValue.DIAMETER_40.toString());
//
//        sizeDao.add(size1);
//        sizeDao.add(size2);
//
//        Map<Size, Long> prices = new HashMap<>();
//        prices.put(size1, new Long(10000));
//        prices.put(size2, new Long(12001));
//
//        Pizza pizza = new Pizza();
//        pizza.setName("Neapolitano");
//        pizza.setPrices(prices);
//        pizza.setDescription("This is the best pizza you have ever seen! " +
//                "This is the best pizza you have ever seen! This is the best pizza you have ever seen!");
//
//        Pizza pizza1 = new Pizza();
//        pizza1.setName("Milana");
//        pizza1.setPrices(new HashMap<>(prices));
//        pizza1.setDescription("This is the best pizza you have ever seen! " +
//                "This is the best pizza you have ever seen! This is the best pizza you have ever seen!");
//
//        pizzaDao.add(pizza);
//        pizzaDao.add(pizza1);
//
//        User admin = new User();
//        admin.setLogin("admin");
//
//        try {
//            MessageDigest digest = MessageDigest.getInstance("SHA-256");
//            digest.update("admin".getBytes());
//            admin.setPassword(Base64Encoder.encode(digest.digest()));
//        } catch (NoSuchAlgorithmException e) {
//            logger.error("No such encryption algorithm was found", e);
//        } catch (IOException e) {
//            logger.error("Cant encrypt password", e);
//        }
//
//        Role adminRole = new Role();
//        adminRole.setRoleName(RoleName.ROLE_ADMIN.toString());
//        roleDao.add(adminRole);
//
//        Role userRole = new Role();
//        userRole.setRoleName(RoleName.ROLE_USER.toString());
//        roleDao.add(userRole);
//
//        admin.setRole(adminRole);
//        userDao.add(admin);
    }
}
