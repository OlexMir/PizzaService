package com.tdea.mkr.jms;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJBException;
import javax.faces.bean.*;
import javax.jms.*;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 * Created by olexiy on 11/28/16.
 */
@ManagedBean(name="postsController", eager = true)
@ApplicationScoped
public class PostController {

    private static final Logger logger = LogManager.getLogger(PostController.class);

    private TopicConnection     connection;
    private TopicPublisher      publisher;
    private TopicSession        session;

    @ManagedProperty(value = "#{bonusController}")
    private BonusController bonusController;

    @PostConstruct
    private void init() {
        try {
            logger.info("Init setting for sending message");
            InitialContext ctx = new InitialContext();
            TopicConnectionFactory factory = (TopicConnectionFactory) ctx.lookup("ConnectionFactory");
            connection = factory.createTopicConnection();
            session = connection.createTopicSession(false, TopicSession.AUTO_ACKNOWLEDGE);
            Topic topic = (Topic) ctx.lookup("topic/test");
            publisher = session.createPublisher(topic);
            connection.start();
        } catch (JMSException | NamingException e) {
            logger.info("Cant init JMS settings for sending message");
            throw new EJBException(e);
        }
    }

    @PreDestroy
    private void free() {
        try {
            logger.info("Try to close connection");
            connection.close();
        } catch (JMSException e) {
            logger.info("Cant close connection");
            throw new EJBException(e);
        }
    }

    public void sendMessage() {
        try {
            logger.info("Try to send message. Check if subscribers are existed");
            if(bonusController.getUserIds().isEmpty()) {
                logger.info("No subscribers found, return");
                return;
            }
            logger.info("Send message from admin to all subscribers");
            TextMessage textMessage = session.createTextMessage(bonusController.getBonusDescription());
            publisher.publish(textMessage);
            logger.info("Clear list of subscribers");
            bonusController.clear();
        } catch (JMSException e) {
            throw new EJBException(e);
        }
    }

    public BonusController getBonusController() {
        return bonusController;
    }

    public void setBonusController(BonusController bonusController) {
        this.bonusController = bonusController;
    }
}
