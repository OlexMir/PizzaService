package com.tdea.mkr.jms;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by olexiy on 11/29/16.
 */
@ManagedBean
@ApplicationScoped
public class BonusController {

    private final static Logger logger = LogManager.getLogger(BonusController.class);

    //Init value for Bonus description
    private String bonusDescription = "Get 2% super bonus";

    //Init value for Bonus post information
    private String postContent = "Which of these would you find most " +
            "satisfying as a reward for doing a great day's work at the " +
            "office: a bit of extra cash, a compliment from the boss or " +
            "free pizza?\nIt may come as a surprise, but people actually" +
            " prefer pizza and praise over money.\n";

    private Set<Long> userIds = new HashSet<>();

    public void add(Long userId) {
        userIds.add(userId);
    }

    public boolean contains(Long userId) {
        return userIds.contains(userId);
    }

    public void clear() {
        userIds.clear();
    }

    public Set<Long> getUserIds() {
        return userIds;
    }

    public void setUserIds(Set<Long> userIds) {
        this.userIds = userIds;
    }

    public String getBonusDescription() {
        return bonusDescription;
    }

    public void setBonusDescription(String bonusDescription) {
        this.bonusDescription = bonusDescription;
    }

    public String getPostContent() {
        return postContent;
    }

    public void setPostContent(String postContent) {
        this.postContent = postContent;
    }
}
