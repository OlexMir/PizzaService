package com.tdea.mkr.jms;

import com.tdea.mkr.dao.BonusDao;
import com.tdea.mkr.dao.UserDao;
import com.tdea.mkr.entity.User;
import com.tdea.mkr.entity.Bonus;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.faces.bean.*;
import javax.faces.context.FacesContext;
import javax.jms.*;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 * Created by olexiy on 11/28/16.
 */
@ManagedBean(eager = true)
@SessionScoped
public class PostReceiverController {

    private static final Logger logger = LogManager.getLogger(PostReceiverController.class);

    private TopicConnection     connection;
    private TopicSession        session;
    private TopicSubscriber     subscriber;

    @ManagedProperty(value = "#{bonusController}")
    private BonusController bonusController;

    @EJB
    private UserDao<User> userDao;

    @EJB
    private BonusDao<Bonus> bonusDao;

    @PostConstruct
    private void init() {
        try {
            logger.info("Init setting for receive message");
            InitialContext ctx = new InitialContext();
            TopicConnectionFactory factory = (TopicConnectionFactory) ctx.lookup("ConnectionFactory");
            connection = factory.createTopicConnection();
            session = connection.createTopicSession(false, TopicSession.AUTO_ACKNOWLEDGE);
            Topic topic = (Topic) ctx.lookup("topic/test");
            subscriber = session.createSubscriber(topic);
            connection.start();
        } catch (JMSException | NamingException e) {
            logger.info("Cant init JMS settings for receiving message");
            throw new EJBException(e);
        }
    }

    @PreDestroy
    private void free() {
        try {
            logger.info("Try to close connection");
            connection.close();
        } catch (JMSException e) {
            logger.info("Cant close connection");
            throw new EJBException(e);
        }
    }

    private User getUser() {
        User user = userDao.find(
                FacesContext
                        .getCurrentInstance()
                        .getExternalContext()
                        .getRemoteUser()
        ).get(0);
        return user;
    }

    public void receiveMessage() {
        try {
            logger.info("Try to receive message from admin");
            User user = getUser();
            if(bonusController.contains(user.getId())) {
                logger.info("User already is subscriber");
                return;
            }

            bonusController.add(user.getId());
            TextMessage msg = (TextMessage) subscriber.receive();
            if(msg == null) {
                logger.error("Message null or unreceived");
            } else {
                logger.info("Try to add new bonus to user id=" + user.getId());
                Bonus bonus = new Bonus();
                bonus.setDescription(msg.getText());
                bonus.setUser(user);
                bonus.setActive(true);
                bonusDao.add(bonus);
                logger.info("Bonus added");
            }
        } catch (JMSException e) {
            logger.info("Error while receiving message");
            throw new EJBException(e);
        }
    }

    public UserDao<User> getUserDao() {
        return userDao;
    }

    public void setUserDao(UserDao<User> userDao) {
        this.userDao = userDao;
    }

    public BonusDao<Bonus> getBonusDao() {
        return bonusDao;
    }

    public void setBonusDao(BonusDao<Bonus> bonusDao) {
        this.bonusDao = bonusDao;
    }

    public BonusController getBonusController() {
        return bonusController;
    }

    public void setBonusController(BonusController bonusController) {
        this.bonusController = bonusController;
    }
}
