package com.tdea.mkr.service;

import com.tdea.mkr.dao.*;
import com.tdea.mkr.entity.*;
import com.tdea.mkr.entity.util.OrderStatus;
import com.tdea.mkr.entity.Bonus;
import com.tdea.mkr.model.Item;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import java.util.List;
import java.util.Map;

/**
 * Created by olexiy on 11/21/16.
 */
@Stateless
public class OrderService {

    private static Logger logger = LogManager.getLogger(OrderService.class);

    @EJB
    private OrderDao<UserOrder> orderDao;

    @EJB
    private PizzaDao<Pizza> pizzaDao;

    @EJB
    private SizeDao<Size> sizeDao;

    @EJB
    private OrderRowDao<OrderRow> orderRowDao;

    @EJB
    private BonusDao<Bonus> bonusDao;

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void createOrder(UserOrder userOrder, Map<Item, Integer> pizzasInCart) {
        orderDao.add(userOrder);
        Long total = Long.valueOf(0);

        logger.info("Check if order id=" + userOrder.getUserId() + " has bonus");

        Bonus bonus = userOrder.getBonus();
        if(bonus != null) {
            logger.info("Set bonus inactive, bonus id=" + bonus.getId());
            bonus.setActive(false);
            bonusDao.update(bonus);
        }

        for(Item item : pizzasInCart.keySet()) {
            Pizza pizza         = pizzaDao.find(item.getId());
            Size size           = sizeDao.findByName(item.getSize());
            Long pizzaPrice     = pizza.getPrices().get(size);
            Integer pizzaCount  = pizzasInCart.get(item);
            Long totalForItem   = calculateTotalForItem(pizzaPrice, pizzaCount);

            OrderRow orderRow = new OrderRow(
                    pizza.getName(), size.getValue(),
                    userOrder, pizzaCount,
                    pizzaPrice, totalForItem
            );

            orderRowDao.add(orderRow);
            total += totalForItem;
        }

        userOrder.setTotal(total);
        orderDao.update(userOrder);
        pizzasInCart.clear();
    }

    public Long calculateTotalForItem(Long pizzaPrice, Integer pizzaCount) {
        logger.info("Calculate total for item");
        Long result = new Long(pizzaCount * pizzaPrice);
        return result;
    }

    public List<UserOrder> findAllOrders(Long id) {
        logger.info("Find all orders by user id=" + id);
        return orderDao.findAllOrdersByUserId(id);
    }

    public List<UserOrder> findAllProcessingOrders() {
        logger.info("Find all orders with status PROCESSING");
        String processingStatus = OrderStatus.PROCESSING.toString();
        return orderDao.findOrdersByStatus(processingStatus);
    }

    public List<UserOrder> findAllConfirmedOrders() {
        logger.info("Find all orders with status CONFIRMED");
        String confirmedStatus = OrderStatus.CONFIRMED.toString();
        return orderDao.findOrdersByStatus(confirmedStatus);
    }

    public void confirmOrder(Long userOrderId) {
        UserOrder userOrder = orderDao.find(userOrderId);
        String confirmedStatus = OrderStatus.CONFIRMED.toString();
        userOrder.setStatus(confirmedStatus);
        logger.info("Confirm order. Try to update status for order id=" + userOrderId);
        orderDao.update(userOrder);
        logger.info("Updated" + userOrder.toString());
    }

    public void cancelOrder(Long userOrderId) {
        UserOrder userOrder = orderDao.find(userOrderId);
        logger.info("Try to remove order with id=" + userOrderId);
        for (OrderRow row : orderRowDao.findOrderRowsByOrderId(userOrderId)) {
            orderRowDao.delete(row);
        }
        orderDao.delete(userOrder);
        logger.info("Removed order with id=" + userOrderId);
    }
}
