package com.tdea.mkr.entity.util;

/**
 * Created by olexiy on 11/20/16.
 */
public enum SizeValue {
    DIAMETER_30("30"),
    DIAMETER_40("40");

    private final String value;

    SizeValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
