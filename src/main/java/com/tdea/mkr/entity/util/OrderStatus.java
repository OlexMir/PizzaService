package com.tdea.mkr.entity.util;

/**
 * Created by olexiy on 11/20/16.
 */
public enum OrderStatus {
    PROCESSING("PROCESSING"),
    CONFIRMED("CONFIRMED");

    private final String value;

    OrderStatus(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
