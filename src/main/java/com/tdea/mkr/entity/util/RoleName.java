package com.tdea.mkr.entity.util;

/**
 * Created by olexiy on 11/26/16.
 */
public enum RoleName {
    ROLE_USER("ROLE_USER"),
    ROLE_ADMIN("ROLE_ADMIN");

    private final String value;

    RoleName(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
