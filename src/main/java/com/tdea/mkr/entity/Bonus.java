package com.tdea.mkr.entity;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.persistence.*;

/**
 * Created by olexiy on 11/28/16.
 */
@Entity
@Table(name = "BONUSES")
public class Bonus extends GenericEntity {

    private final static Logger logger = LogManager.getLogger(Bonus.class);

    @Column(name = "DESCRIPTION")
    private String description;

    @ManyToOne
    @JoinColumn(name = "USER_ID")
    private User user;

    @Column(name = "ACTIVE_STATUS")
    private boolean active;

    @Override
    public String toString() {
        return "Bonus{" +
                "description='" + description + '\'' +
                ", user=" + user +
                ", active=" + active +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Bonus bonus = (Bonus) o;

        if (description != null ? !description.equals(bonus.description) : bonus.description != null) return false;
        return user != null ? user.equals(bonus.user) : bonus.user == null;

    }

    @Override
    public int hashCode() {
        int result = description != null ? description.hashCode() : 0;
        result = 31 * result + (user != null ? user.hashCode() : 0);
        return result;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
