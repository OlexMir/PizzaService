package com.tdea.mkr.entity;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.persistence.*;

/**
 * Created by olexiy on 11/20/16.
 */
@Entity
@Table(name = "SIZES")
public class Size extends GenericEntity {

    private final static Logger logger = LogManager.getLogger(Size.class);

    @Column(name = "SIZE_VALUE")
    private String value;

    public Size() {}

    public Size(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Size size = (Size) o;

        if (id != null ? !id.equals(size.id) : size.id != null) return false;
        return value != null ? value.equals(size.value) : size.value == null;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (value != null ? value.hashCode() : 0);
        return result;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String name) {
        this.value = name;
    }
}
