package com.tdea.mkr.entity;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.persistence.*;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Created by olexiy on 11/20/16.
 */
@Entity
@Table(name = "PIZZA")
public class Pizza extends GenericEntity {

    private final static Logger logger = LogManager.getLogger(Pizza.class);

    @Column(name = "PIZZA_NAME")
    private String name;

    @ElementCollection
    @JoinTable(name = "PIZZA_PRICES", joinColumns = @JoinColumn(name = "PIZZA_ID"))
    @MapKeyJoinColumn(name = "SIZE_ID", referencedColumnName = "ID")
    @Column(name = "PRICE")
    private Map<Size, Long> prices = new HashMap<>();

    @Column(name = "DESCRIPTION")
    private String description;

    @Override
    public String toString() {
        return "Pizza{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Pizza pizza = (Pizza) o;

        if (id != null ? !id.equals(pizza.id) : pizza.id != null) return false;
        return name != null ? name.equals(pizza.name) : pizza.name == null;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map<Size, Long> getPrices() {
        return prices;
    }

    public void setPrices(Map<Size, Long> prices) {
        this.prices = prices;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
