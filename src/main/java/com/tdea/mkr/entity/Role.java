package com.tdea.mkr.entity;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.persistence.*;

/**
 * Created by olexiy on 11/25/16.
 */
@Entity
@Table(name = "ROLES")
public class Role extends GenericEntity {

    private final static Logger logger = LogManager.getLogger(Role.class);

    @Column(name = "ROLE_NAME")
    private String roleName;

    public Role() {}

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }
}
