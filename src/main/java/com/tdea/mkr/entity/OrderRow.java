package com.tdea.mkr.entity;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.persistence.*;

/**
 * Created by olexiy on 11/21/16.
 */
@Entity
@Table(name = "ORDER_ROWS")
public class OrderRow extends GenericEntity {

    private final static Logger logger = LogManager.getLogger(OrderRow.class);

    @Column(name = "PIZZA_NAME")
    private String pizzaName;

    @Column(name = "SIZE_VALUE")
    private String sizeValue;

    @ManyToOne(targetEntity = UserOrder.class)
    @JoinColumn(name = "ORDER_ID")
    private UserOrder userOrder;

    @Column(name = "PIZZA_COUNT")
    private Integer count;

    @Column(name = "PIZZA_PRICE")
    private Long price;

    @Column(name = "PIZZA_TOTAL")
    private Long total;

    public OrderRow() {}

    public OrderRow(String pizzaName, String sizeValue, UserOrder userOrder, Integer count, Long price, Long total) {
        this.pizzaName = pizzaName;
        this.sizeValue = sizeValue;
        this.userOrder = userOrder;
        this.count = count;
        this.price = price;
        this.total = total;
    }

    @Override
    public String toString() {
        return "OrderRow{" +
                "id=" + id +
                ", pizzaName=" + pizzaName +
                ", sizeValue=" + sizeValue +
                ", userOrder=" + userOrder +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OrderRow orderRow = (OrderRow) o;

        if (id != null ? !id.equals(orderRow.id) : orderRow.id != null) return false;
        if (pizzaName != null ? !pizzaName.equals(orderRow.pizzaName) : orderRow.pizzaName != null) return false;
        if (sizeValue != null ? !sizeValue.equals(orderRow.sizeValue) : orderRow.sizeValue != null) return false;
        return userOrder != null ? userOrder.equals(orderRow.userOrder) : orderRow.userOrder == null;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (pizzaName != null ? pizzaName.hashCode() : 0);
        result = 31 * result + (sizeValue != null ? sizeValue.hashCode() : 0);
        result = 31 * result + (userOrder != null ? userOrder.hashCode() : 0);
        return result;
    }

    public String getPizzaName() {
        return pizzaName;
    }

    public void setPizzaName(String pizzaName) {
        this.pizzaName = pizzaName;
    }

    public String getSizeValue() {
        return sizeValue;
    }

    public void setSizeValue(String sizeValue) {
        this.sizeValue = sizeValue;
    }

    public UserOrder getUserOrder() {
        return userOrder;
    }

    public void setUserOrder(UserOrder userOrder) {
        this.userOrder = userOrder;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
}
